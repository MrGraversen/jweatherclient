package main;

import java.sql.SQLException;

/**
 * This singleton class manages database connections. Typically there will only be one, but the system can hold as many connections as needed.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class DatabaseManager extends Manager<String>
{
	private static DatabaseManager databaseManagerInstance = null;

	/**
	 * Gets the database manager instance.
	 *
	 * @return the database manager instance
	 */
	public static DatabaseManager getDatabaseManagerInstance()
	{
		if (databaseManagerInstance == null) databaseManagerInstance = new DatabaseManager();

		return databaseManagerInstance;
	}

	/**
	 * Register a database connection with the system.
	 *
	 * @param databaseConnection
	 *            the database connection
	 */
	public void registerDatabase(DatabaseConnection databaseConnection)
	{
		putEntity(databaseConnection);
	}

	/**
	 * Unregister a database and shut it down, making sure the resources are freed.
	 *
	 * @param identifier
	 *            the identifier of the database
	 * @throws SQLException
	 *             SQL exception indicating an error with the database
	 */
	public void unregisterDatabase(String identifier) throws MysqlException
	{
		DatabaseConnection dbc = getDatabaseConnection(identifier);

		if (dbc == null) return;

		dbc.shutdown();
		removeEntity(dbc);
	}

	/**
	 * Gets a database connection registered with the system.
	 *
	 * @param identifier
	 *            the identifier of the database
	 * @return the database connection
	 */
	public DatabaseConnection getDatabaseConnection(String identifier)
	{
		return (DatabaseConnection) getEntity(identifier);
	}
}

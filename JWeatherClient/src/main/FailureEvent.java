package main;

/**
 * This class constitutes data for a serial event that was a failure somehow.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class FailureEvent extends BaseEvent
{
	private final SerialException exception;

	public FailureEvent(SerialException exception)
	{
		this.exception = exception;
	}

	/**
	 * Returns the <code>Exception</code> instance that describes the error that occurred.
	 *
	 * @return the exception
	 */
	public SerialException getError()
	{
		return exception;
	}

	/**
	 * Gets the message that describes the error (the exception message).
	 *
	 * @return the message from the exception
	 */
	@Override
	public String getMessage()
	{
		return "[JDuino] ERROR: " + exception.getMessage();
	}
}
package main;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * This class provides a table-like interaction for operating on result data from a database. <br>
 * The reason the Java implementation ({@code ResultSet} is not used is because it is not as easy to traverse and it needs to closed (throwing an SqlException), leaking MySQL dependencies into other parts of the system.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class DatabaseResultEntity
{
	private LinkedHashMap<String, Object[]> values;
	private int rowCount = 0;
	private int columnCount = 0;
	private String[] columns;

	/**
	 * Instantiates a new database result entity, building the internal data structure from a {@code ResultSet} object.
	 *
	 * @param rs
	 *            the ResultSet to transform
	 */
	public DatabaseResultEntity(ResultSet rs)
	{
		values = new LinkedHashMap<String, Object[]>();

		try
		{
			if (rs.isClosed()) return; // Resultset is closed, get out of here
			if (!rs.next()) return; // No rows in resultset

			rs.last();
			rowCount = rs.getRow();

			rs.absolute(1);
			ResultSetMetaData rsmd = rs.getMetaData();
			columnCount = rsmd.getColumnCount();

			columns = new String[columnCount];

			for (int i = 1; i <= columnCount; i++)
			{
				Object[] rows = new Object[rowCount];

				for (int j = 1; j <= rowCount; j++)
				{
					rs.absolute(j);
					rows[j - 1] = rs.getObject(i);
				}
				columns[i - 1] = rsmd.getColumnName(i);
				values.put(rsmd.getColumnName(i), rows);
			}
		}
		catch (SQLException e)
		{
			if (!e.getMessage().equals("ResultSet is from UPDATE. No Data.")) e.printStackTrace();
		}
		catch (NullPointerException e)
		{

		}
	}

	/**
	 * Gets all the rows from the result structure, by a certain column name.
	 *
	 * @param columnName
	 *            the column name
	 * @return the rows
	 */
	public Object[] getRows(String columnName)
	{
		return values.get(columnName);
	}

	/**
	 * Gets all the rows from the result structure, by a certain column index.
	 *
	 * @param columnIndex
	 *            the column index
	 * @return the rows
	 */
	public Object[] getRows(int columnIndex)
	{
		// ArrayList<String> keys = new ArrayList<String>(values.keySet());

		// return values.get(keys.get(columnIndex));
		return values.get(columns[columnIndex]);
	}

	/**
	 * Gets the row count.
	 *
	 * @return the row count
	 */
	public int getRowCount()
	{
		return rowCount;
	}

	/**
	 * Gets the column names.
	 *
	 * @return the column names
	 */
	public String[] getColumnNames()
	{
		String[] columns = new String[values.size()];

		int i = 0;
		for (Entry<String, Object[]> entry : values.entrySet())
		{
			columns[i] = entry.getKey();
			i++;
		}

		return columns;
	}

	public Object[] getRow(int rowIndex)
	{
		Object[] row = new Object[columnCount];

		for (int i = 0; i < columnCount; i++)
		{
			row[i] = getRows(i)[rowIndex];
		}

		return row;
	}

	public Object[][] getTableAsObjectArray()
	{
		Object[][] table = new Object[rowCount][columnCount];

		for (int i = 0; i < rowCount; i++)
		{
			Object[] row = getRow(i);

			for (int j = 0; j < row.length; j++)
			{
				table[i][j] = row[j];
			}
		}

		return table;
	}
}

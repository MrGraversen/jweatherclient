package main;

/**
 * This interfaces associates a class with the ability to be "managed" by a "manager" class.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public interface Manageable<T>
{
	/**
	 * Gets the identifier of the manageable object.
	 *
	 * @return the identifier
	 */
	public T getIdentifier();
}

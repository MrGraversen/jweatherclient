package main;

/**
 * This class constitutes data for a serial event that was successful, and that contains some form of communication, i.e. a message is transmitted.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class CommunicationEvent extends BaseEvent
{
	private final String message;
	private final long timestamp;
	private final long sequenceNumber;

	public CommunicationEvent(String message, long sequenceNumber)
	{
		this.message = message;
		this.timestamp = System.currentTimeMillis();
		this.sequenceNumber = sequenceNumber;
	}

	/**
	 * Returns a message that is either the data transmitted over serial from an external device, or a message related to something that happened internally.
	 *
	 * @return the message
	 */
	@Override
	public String getMessage()
	{
		return message;
	}

	/**
	 * Returns the timestamp that describes when the event was raised.
	 *
	 * @return the timestamp
	 */
	public long getTimestamp()
	{
		return timestamp;
	}

	/**
	 * Returns the sequence number of this event, i.e. which number in sequence this event is.
	 *
	 * @return the exception
	 */
	public long getSequenceNumber()
	{
		return sequenceNumber;
	}

	public String toString()
	{
		return "[Seq=" + sequenceNumber + "] " + message;
	}
}
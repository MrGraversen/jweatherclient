package main;

/**
 * This interfaces allows a class to be registered as a "serial observer" that will receive events from the serial link.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public interface ISerialObserver
{
	/**
	 * Occurs whenever something is sent over serial from an external device. Can be cast to {@link CommunicationEvent}.
	 *
	 * @param serialEvent
	 *            The event object associated with the event
	 */
	public void onSerialCommunication(ISerialEvent serialEvent);

	/**
	 * Occurs whenever a serial transmission failed. Can be cast to {@link FailureEvent}.
	 *
	 * @param serialEvent
	 *            The event object associated with the event
	 */
	public void onCommunicationError(ISerialEvent serialEvent);

	/**
	 * Occurs when the serial link is established successfully. Can be cast to {@link CommunicationEvent}.
	 *
	 * @param serialEvent
	 *            The event object associated with the event
	 */
	public void onConnectSuccess(ISerialEvent serialEvent);

	/**
	 * Occurs when the serial link fails to establish. Can be cast to {@link FailureEvent}.
	 *
	 * @param serialEvent
	 *            The event object associated with the event
	 */
	public void onConnectError(ISerialEvent serialEvent);

	/**
	 * Occurs if the serial link is gracefully disconnected. Can be cast to {@link CommunicationEvent}.
	 *
	 * @param serialEvent
	 *            The event object associated with the event
	 */
	public void onDisconnect(ISerialEvent serialEvent);
}

package main;
import java.util.Calendar;

/**
 * The purpose of this class is to provide a consistent, easy-to-use console and log output for the developer.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ConsoleHelper
{
	private static boolean enabled = false;
	private static boolean timestampEnabled = true;

	private ConsoleHelper()
	{

	}

	/**
	 * Disable the console helper (no output will be carried out).
	 */
	public static void disable()
	{
		enabled = false;
	}

	/**
	 * Enable the console helper.
	 */
	public static void enable()
	{
		enabled = true;
	}

	/**
	 * Disable timestamp.
	 */
	public static void disableTimestamp()
	{
		timestampEnabled = false;
	}

	/**
	 * Enable timestamp.
	 */
	public static void enableTimestamp()
	{
		timestampEnabled = true;
	}

	private static String getCurrentTime()
	{
		Calendar myDate = Calendar.getInstance();
		myDate.setTimeInMillis(System.currentTimeMillis());

		String hour = null;
		String minute = null;
		String second = null;

		if (myDate.get(Calendar.HOUR_OF_DAY) < 10)
		{
			hour = "0" + myDate.get(Calendar.HOUR_OF_DAY);
		}
		else
		{
			hour = "" + myDate.get(Calendar.HOUR_OF_DAY);
		}

		if (myDate.get(Calendar.MINUTE) < 10)
		{
			minute = "0" + myDate.get(Calendar.MINUTE);
		}
		else
		{
			minute = "" + myDate.get(Calendar.MINUTE);
		}

		if (myDate.get(Calendar.SECOND) < 10)
		{
			second = "0" + myDate.get(Calendar.SECOND);
		}
		else
		{
			second = "" + myDate.get(Calendar.SECOND);
		}

		return hour + ":" + minute + ":" + second;
	}

	/**
	 * Prints a line to the console and, if persistence is enabled, to the log file, with a "sender" name (usually the class the announce was requested from).
	 *
	 * @param sender
	 *            the sender (usually a class name)
	 * @param output
	 *            the output
	 */
	public static void announce(String sender, String output)
	{
		if (enabled)
		{
			if (timestampEnabled)
			{
				output = "[" + sender + " \u2014 " + getCurrentTime() + "]: " + output;
			}
			else
			{
				output = "[" + sender + "]: " + output;
			}

			System.out.println(output);
		}
	}
}

package main;

/**
 * A simple exception class to streamline the naming of related exceptions.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class MysqlException extends Exception
{
	public MysqlException(String errorMessage)
	{
		super(errorMessage);
	}
}
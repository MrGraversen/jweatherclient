package main;

import java.sql.*;

/**
 * This class constitutes a simple MySQL connection to a database specified in the object's constructor. <br>
 * This class also provides simple methods for interacting with the database.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class DatabaseConnection implements Manageable<String>
{
	private final String NAME = this.getClass().getSimpleName();

	private final String SERVER_ADDRESS;

	private final String DATABASE_NAME;

	private final String DATABASE_USERNAME;

	private final String DATABASE_PASSWORD;

	private Connection connection;

	private Statement statement;

	private PreparedStatement preparedStatement;

	private ResultSet rs;

	/**
	 * Instantiates a new database connection.
	 *
	 * @param serverAddress
	 *            the server address
	 * @param databaseName
	 *            the database name
	 * @param databaseUsername
	 *            the database username
	 * @param databasePassword
	 *            the database password
	 * @throws ClassNotFoundException
	 *             if the MySQL driver was not found
	 * @throws SQLException
	 *             if there was an error with connecting to the database
	 */
	public DatabaseConnection(String serverAddress, String databaseName, String databaseUsername, String databasePassword) throws MysqlException
	{
		try
		{
			SERVER_ADDRESS = serverAddress;
			DATABASE_NAME = databaseName;
			DATABASE_USERNAME = databaseUsername;
			DATABASE_PASSWORD = databasePassword;

			Class.forName("com.mysql.jdbc.Driver");
			ConsoleHelper.announce(NAME, "Loaded MySQL driver");
			connection = DriverManager.getConnection("jdbc:mysql://" + SERVER_ADDRESS + "/" + DATABASE_NAME, DATABASE_USERNAME, DATABASE_PASSWORD);
			ConsoleHelper.announce(NAME, "Connected to database " + DATABASE_NAME + " at " + SERVER_ADDRESS);
		}
		catch (Exception e)
		{
			throw new MysqlException("MySQL error: " + e.getMessage());
		}
	}

	/**
	 * Query the database with a MySQL query. <br>
	 * For example: {@code SELECT id, password FROM userTable WHERE username='martin'}
	 *
	 * @param sqlQuery
	 *            the sql query
	 * @return the database result entity containing all the result data (null if no data was returned from the database)
	 * @throws OwnzoneException
	 *             an exception indicating a problem with the query
	 */
	public DatabaseResultEntity call(String sqlQuery) throws MysqlException
	{
		try
		{
			preparedStatement = connection.prepareStatement(sqlQuery);

			String command = sqlQuery.split(" ")[0].toUpperCase();
			boolean dataManipulation = command.equalsIgnoreCase("INSERT") || command.equalsIgnoreCase("UPDATE") || command.equalsIgnoreCase("DELETE");

			if (dataManipulation)
			{
				preparedStatement.execute();
			}
			else
			{
				rs = preparedStatement.executeQuery();
			}

			return new DatabaseResultEntity(rs);
		}
		catch (SQLException e)
		{
			throw new MysqlException("MySQL error: " + e.getMessage());
		}
		finally
		{
			close(rs, preparedStatement);
		}
	}

	/**
	 * Call a batch of queries against the database.
	 *
	 * @param queries
	 *            the queries
	 * @throws OwnzoneException
	 *             an exception indicating a problem with the queries
	 */
	public void callBatch(String[] queries) throws MysqlException
	{
		try
		{
			statement = connection.createStatement();

			for (String query : queries)
			{
				statement.addBatch(query);
			}

			statement.executeBatch();
		}
		catch (SQLException e)
		{
			throw new MysqlException("MySQL error: " + e.getMessage());
		}
		finally
		{
			close(rs, statement);
		}
	}

	/**
	 * Call a stored procedure in the database. <br>
	 * The method transforms the arguments into a stored procedure query. The following example describes a hypothetical login procedure: <br>
	 * {@code callStoredProcedure("login", "martin", "abc1234");} <br>
	 * Turns into: {@code CALL login('martin', 'abc1234');} <br>
	 * And is sent to the database.
	 *
	 * @param storedProcedureName
	 *            the stored procedure name
	 * @param argument
	 *            the arguments for the stored procedure
	 * @return the database result entity containing all the result data (null if no data was returned from the database)
	 * @throws OwnzoneException
	 *             an exception indicating a problem with the queries
	 */
	public DatabaseResultEntity callStoredProcedure(String storedProcedureName, String... argument) throws MysqlException
	{
		try
		{
			int numberOfArguments = argument.length;

			String sqlQuery = "CALL " + storedProcedureName + "(";

			for (int i = 0; i < numberOfArguments; i++)
			{
				sqlQuery = sqlQuery + "?,";
			}

			sqlQuery = (numberOfArguments > 0 ? (sqlQuery.substring(0, sqlQuery.length() - 1) + ");") : (sqlQuery = sqlQuery + ");"));

			preparedStatement = connection.prepareStatement(sqlQuery);

			int i = 1;
			for (String s : argument)
			{
				preparedStatement.setString(i, s);
				i++;
			}

			rs = preparedStatement.executeQuery();

			return new DatabaseResultEntity(rs);
		}
		catch (SQLException e)
		{
			throw new MysqlException("MySQL error: " + e.getMessage());
		}
		finally
		{
			close(rs, preparedStatement);
		}
	}

	/**
	 * Close the ResultSet and Statement variables, freeing up resources. <br>
	 * ResultSet and Statement objects that loses their references are not garbage collected, therefore they must be closed.
	 *
	 * @param resultSet
	 *            the ResultSet object
	 * @param statement
	 *            the Statement object
	 */
	private void close(ResultSet resultSet, Statement statement)
	{
		if (resultSet != null)
		{
			try
			{
				resultSet.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}

		if (statement != null)
		{
			try
			{
				statement.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see abstracts.Manageable#getIdentifier()
	 */
	@Override
	public String getIdentifier()
	{
		return DATABASE_NAME;
	}

	/**
	 * Shutdown the connection to the database.
	 *
	 * @throws SQLException
	 *             SQL exception indicating a database error
	 */
	public void shutdown() throws MysqlException
	{
		try
		{
			connection.close();
		}
		catch (SQLException e)
		{
			throw new MysqlException("MySQL error: " + e.getMessage());
		}
	}

	public boolean isConnected()
	{
		try
		{
			return !connection.isClosed();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
}

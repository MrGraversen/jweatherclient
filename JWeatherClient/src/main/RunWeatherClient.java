package main;
public class RunWeatherClient
{
	private ISerialObserver serialObserver;
	private DatabaseManager dbManager;

	private boolean dbReady = false;

	public RunWeatherClient()
	{
		serialObserver = new ISerialObserver()
		{
			@Override
			public void onSerialCommunication(ISerialEvent serialEvent)
			{
				serialEvent.print();

				if (!dbReady) return;

				try
				{
					String[] parts = serialEvent.getMessage().split(":");
					dbManager.getDatabaseConnection("weather").callStoredProcedure("submit", parts[0], parts[1]);
				}
				catch (MysqlException e)
				{
					e.printStackTrace();
				}
			}

			@Override
			public void onDisconnect(ISerialEvent serialEvent)
			{
				serialEvent.print();
				
				try
				{
					dbManager.getDatabaseConnection("weather").shutdown();
				}
				catch (MysqlException e)
				{
					e.printStackTrace();
				}
			}

			@Override
			public void onConnectSuccess(ISerialEvent serialEvent)
			{
				serialEvent.print();
				
				initializeDatabase();
			}

			@Override
			public void onConnectError(ISerialEvent serialEvent)
			{
				serialEvent.print();
			}

			@Override
			public void onCommunicationError(ISerialEvent serialEvent)
			{
				serialEvent.print();
			}
		};

		SerialInterface serialInterface = new SerialInterface(9600);
		serialInterface.addObserver(serialObserver);

		serialInterface.connect();

		// Run until process is shut down, quick and dirty
	}

	private void initializeDatabase()
	{
		try
		{
			dbManager = DatabaseManager.getDatabaseManagerInstance();
			dbManager.putEntity(new DatabaseConnection("martinbytes.com", "weather", "weather", "weather"));
			dbReady = true;
		}
		catch (MysqlException e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		ConsoleHelper.enable();
		new RunWeatherClient();
	}

//	private void sleep(int n)
//	{
//		try
//		{
//			Thread.sleep(n);
//		}
//		catch (InterruptedException e)
//		{
//			e.printStackTrace();
//		}
//	}
}
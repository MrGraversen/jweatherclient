package main;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * This abstract class enables a subclass to "manage" other objects (implementing the {@code Manageable} interface). 
 * This includes holding the objects and doing simple operations on the objects.
 *
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 * @param <T> the generic type
 */
public abstract class Manager<T>
{
	private LinkedHashMap<T, Manageable<T>> entities;

	public Manager()
	{
		entities = new LinkedHashMap<>();
	}

	/**
	 * Put a manageable entity into the holder.
	 *
	 * @param entity the entity to be added
	 */
	protected void putEntity(Manageable<T> entity)
	{
		entities.put((T) entity.getIdentifier(), entity);
	}

	/**
	 * Gets an entity by its identifier.
	 *
	 * @param identifier the identifier of the object
	 * @return the entity
	 */
	protected Manageable<T> getEntity(T identifier)
	{
		if (!entities.containsKey(identifier)) return null;
		return entities.get(identifier);
	}

	/**
	 * Gets the all entities in an ArrayList.
	 *
	 * @return ArrayList containing all the entities held by this Manager
	 */
	protected ArrayList<Manageable<T>> getAllEntities()
	{
		ArrayList<Manageable<T>> listOfEntities = new ArrayList<Manageable<T>>();

		for (Entry<T, Manageable<T>> entity : entities.entrySet())
		{
			listOfEntities.add(entity.getValue());
		}

		return listOfEntities;
	}

	/**
	 * Removes an entity from this data holder.
	 *
	 * @param entity the entity to be removed
	 */
	protected void removeEntity(Manageable<T> entity)
	{
		entities.remove(entity.getIdentifier());
	}

	/**
	 * Gets the number of entities.
	 *
	 * @return the number of entities
	 */
	protected int getNumberOfEntities()
	{
		return entities.size();
	}
}

package test;

import main.Manageable;

public class DummyEntity implements Manageable<String>
{
	private String identifier;

	public DummyEntity(String identifier)
	{
		this.identifier = identifier;
	}

	@Override
	public String getIdentifier()
	{
		return identifier;
	}
}
package test;

import java.util.ArrayList;

import main.Manageable;
import main.Manager;

public class DummyManager<T> extends Manager<T>
{
	public void putDummyEntity(Manageable<T> entity)
	{
		putEntity(entity);
	}

	public Manageable<T> getDummyEntity(T identifier)
	{
		return getEntity(identifier);
	}

	public ArrayList<Manageable<T>> getAllDummyEntities()
	{
		return getAllEntities();
	}

	public void removeDummyEntity(Manageable<T> entity)
	{
		removeEntity(entity);
	}

	public int getNumberOfDummyEntities()
	{
		return getNumberOfEntities();
	}
}
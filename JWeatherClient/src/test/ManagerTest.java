package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import main.Manageable;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ManagerTest
{
	private DummyManager<String> manager;
	private final int entityCount = 10;

	@Before
	public void setUp() throws Exception
	{
		// Arrange
		manager = new DummyManager<>();
	}

	@Test
	public void testAEntityCount()
	{
		// Act
		for (int i = 0; i < entityCount; i++)
		{
			manager.putDummyEntity(new DummyEntity("entity" + i));
		}

		// Assert
		assertTrue(manager.getNumberOfDummyEntities() == entityCount);
	}

	@Test
	public void testBGetEntity()
	{
		// Act
		for (int i = 0; i < entityCount; i++)
		{
			manager.putDummyEntity(new DummyEntity("entity" + i));
		}
		DummyEntity de = (DummyEntity) manager.getDummyEntity("entity2");

		// Assert
		assertTrue(de != null);
	}

	@Test
	public void testCMatchAllElements()
	{
		ArrayList<DummyEntity> listOfDummies = new ArrayList<>();

		// Act
		for (int i = 0; i < entityCount; i++)
		{
			DummyEntity de = new DummyEntity("entity" + i);

			listOfDummies.add(de);
			manager.putDummyEntity(de);
		}

		// Assert
		int i = 0;
		for (Manageable<String> m : manager.getAllDummyEntities())
		{
			if (!(m instanceof DummyEntity)) fail("DummyEntity typecast fail");

			DummyEntity de = (DummyEntity) m;

			assertTrue(listOfDummies.get(i).getIdentifier().equals(de.getIdentifier()));
			i++;
		}
	}

	@After
	public void tearDown() throws Exception
	{

	}
}

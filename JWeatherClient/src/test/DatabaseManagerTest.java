package test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import main.DatabaseConnection;
import main.DatabaseManager;
import main.MysqlException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatabaseManagerTest
{
	private DatabaseManager dbm;
	private DatabaseConnection dc;

	@Before
	public void setUp() throws Exception
	{
		dbm = DatabaseManager.getDatabaseManagerInstance();
	}

	@After
	public void tearDown() throws Exception
	{
		// Nothing
	}

	@Test(timeout=1000)
	public void testADatabaseConnectionInstanceRegister()
	{
		// Act
		try
		{
			dc = new DatabaseConnection("martinbytes.com", "weather", "weather", "weather");
			dbm.registerDatabase(dc);
		}
		catch (MysqlException e)
		{
			fail(e.getMessage());
		}

		// Assert
		assertTrue(dbm.getDatabaseConnection("weather") != null);
	}

	@Test(timeout=1000)
	public void testCDatabaseConnectionInstanceUnregister()
	{
		// Act
		try
		{
			dc = new DatabaseConnection("martinbytes.com", "weather", "weather", "weather");
			dbm.registerDatabase(dc);
			dbm.unregisterDatabase("weather");
		}
		catch (MysqlException e)
		{
			fail(e.getMessage());
		}

		// Assert
		assertTrue(dbm.getDatabaseConnection("weather") == null);
	}

	@Test(timeout=1000)
	public void testBDatabaseConnectionShutdown()
	{
		// Act
		try
		{
			dc = new DatabaseConnection("martinbytes.com", "weather", "weather", "weather");
			dc.shutdown();
		}
		catch (MysqlException e)
		{
			fail(e.getMessage());
		}

		// Assert
		assertTrue(!dc.isConnected());
	}
}